#!/usr/bin/env python

import easywebdav
import os
import shutil
import sys
import click



@click.group(help=r"""Deploy to Blackboard
This program will build the git to the build folder,
clean the old files from blackboard and
deploy the specific folder to blackboard.

The program expects the environment variables BBSERVER, BBUSERNAME, BBSECRET, BBPROTOCOL to be available for connecting to Blackboard. 
BBBASEPATH and BBCOURSE are needed for creating the correct remote path, where BBCOURSE can be overruled by the cli argument.
""")
@click.version_option('0.1.0')
@click.option('--debug/--no-debug', '-d', default=False)
@click.pass_context
def bbci(ctx, debug):
    """click group, set context and main object"""
    ctx.obj['DEBUG'] = debug


@bbci.command(help="build the exercises to the specified folder.")
@click.option('--source', required=True, help="Give the local folder")
@click.option('--dir', required=True, help="Give the remote folder")
@click.option('--keyword', required=True, help="Give the filter keyword [<keyword>.*]")
@click.pass_context
def build(ctx,source: str,dir: str, keyword: str):
    """See command Help"""
    for subdir, dirs, files in os.walk(source):
        for file in files:
            if keyword in file:
                src = os.path.join(subdir, file)
                dstdir = subdir.replace(source,dir)
                dst = os.path.join(dstdir, file)
                if not os.path.isdir(dstdir):
                    os.makedirs(dstdir)
                shutil.copy(src,dst)


@bbci.command(help="clean the specified remote folder on BB.")
@click.option('--dir', required=True, help="Give the remote folder")
@click.option('--course', required=False, help="Give the BB course name")
@click.pass_context
def clean(ctx,dir: str, course: str):
    webdav = connect()
    path = buildRemotePath(dir, course)
    
    webdav.rmdir(path)
    webdav.mkdir(path)


@bbci.command(help="deploy the build to the specified dir on Blackboard.")
@click.option('--source', required=True)
@click.option('--dir', required=True, help="Give the remote folder")
@click.option('--course', required=False, help="Give the BB course name")
@click.pass_context
def deploy(ctx,source: str, dir: str, course: str):
    webdav = connect()
    # path = buildRemotePath(dir, course)

    for subdir, dirs, files in os.walk(source):
        for file in files:
            dstdir = subdir.replace(source,dir)
            path = buildRemotePath(dstdir, course)
            webdav.mkdir(path)
            dst = os.path.join(path, file)
            src = os.path.join(subdir, file)
            webdav.upload(src,dst)


def buildRemotePath(dir: str, course: str):
    if course is None:
        course = os.environ['BBCOURSE']
    return os.path.join(os.environ['BBBASEPATH'],course,dir)

def connect():
    # connecting to the webdav and instantiating the webdav variable
    return easywebdav.connect(
        os.environ['BBSERVER'],
        username=os.environ['BBUSERNAME'],
        password=os.environ['BBSECRET'],
        protocol=os.environ['BBPROTOCOL'])

def debug_mode():
    """Guess if we are in debug mode, useful to display runtime errors"""
    if '--debug' in sys.argv or '-d' in sys.argv:
        return True
    return False


def main():
    """Main function when the CLI Script is called directly"""

    try:
        bbci(obj={})
    except Exception as error:
        msg = click.style(r""" ______ _____  _____   ____  _____
|  ____|  __ \|  __ \ / __ \|  __ \
| |__  | |__) | |__) | |  | | |__) |
|  __| |  _  /|  _  /| |  | |  _  /
| |____| | \ \| | \ \| |__| | | \ \
|______|_|  \_\_|  \_\\____/|_|  \_\

""", fg='yellow')
        msg += click.style('{}'.format(error), fg='red')
        print(msg + '\n', file=sys.stderr)

        if debug_mode() is True:
            raise error

        sys.exit(1)


if __name__ == '__main__':
    main()


#     build("Solutions","Exercises","Opdracht")
#     clean("bbcswebdav/users/bindelsrjm/Opdrachten")
#     deploy("Exercises","bbcswebdav/users/bindelsrjm/Opdrachten")